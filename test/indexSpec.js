/* eslint-disable no-unused-expressions */

const chai = require('chai');
const request = require('request-promise-native');
const sinon = require('sinon');
const index = require('../index');

const { expect } = chai;

describe('coding challenge spec', () => {
  it('should return an array of english articles when requested', async () => {
    // Add implementation
  });

  it('should return an array of spanish articles when requested', async () => {
    // Add implmentation
  });

  it('should return an array of all articles when no language is specified', async () => {
    // Add implementation
  });
});
